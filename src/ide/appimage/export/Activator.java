package ide.appimage.export;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "ide.appimage.export"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	public static IProject getSelectedProject() {
		try {
			IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		    if (window != null)
		    {
		        IStructuredSelection selection = (IStructuredSelection) window.getSelectionService().getSelection();
		        Object firstElement = selection.getFirstElement();
		        if (firstElement instanceof IResource)
		        {
		        	return ((IResource) selection.getFirstElement()).getProject(); //(IProject)((IAdaptable)firstElement).getAdapter(IProject.class);
		        }else {
		        	return null;
		        }
		    }else {
		    	return null;
		    }
		} catch (Exception exception) {
			return null;
		}
	}
	
	public static String getSelectedProjectName() {
		try {
			return getSelectedProject().getName();
		} catch (Exception exception) {
			return "";
		}
	}
	
	public static String getProjectPath() {
		try {
			return String.valueOf(getSelectedProject().getLocation()) + System.getProperty("file.separator");
		} catch (Exception exception) {
			return "";
		}		
	}
	
	public static String getPlatformPath() {
		return Platform.getInstallLocation().getURL().getPath();
	}
	
	public static String getWorkspacePath() {
		return ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString() + System.getProperty("file.separator");
	}
	
	public static String getPluginPath() {
		return Activator.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	}
	
	public static String readTextFile(String file_path) 
	{
		StringBuilder contentBuilder = new StringBuilder();
		try (Stream<String> stream = Files.lines(Paths.get(file_path), StandardCharsets.UTF_8)) 
		{
			stream.forEach(s -> contentBuilder.append(s).append("\n"));
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return contentBuilder.toString();
	}
}
