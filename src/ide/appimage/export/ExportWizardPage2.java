package ide.appimage.export;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.util.converters.XMLConverter;
import org.eclipse.swt.widgets.Combo;

public class ExportWizardPage2 extends WizardPage {
	private Text txt_name;
	private Text txt_version;
	private Text txt_icon;
	private Text txt_main_class;
	private Text txt_output_dir;
	private Tree tree_classpath;
	private String var_src;
	private CTabFolder tabs;
	private String project_path;
	private String project_name;
	private String architecture = "x86_64";
	private CTabItem tab_classpath;
	private CTabItem tab_general;
	private Composite cmp_general;
	private Composite cmp_classpath;
	
	private String log = "";
	private Combo txt_integration;
	
	/**
	 * Create the wizard.
	 */
	public ExportWizardPage2(String project_path, String project_name) {
		super("wizardPage");
		setPageComplete(false);
		this.project_path = project_path;
		this.project_name = project_name;
		setImageDescriptor(ImageDescriptor.createFromImage(new Image(null, getClass().getResourceAsStream("/ide/appimage/export/appimage_64.png"))));
		setTitle("Export AppImage");
		setDescription("Export an executable Linux (x86_64) compatible AppImage archive.");
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);

		setControl(container);
		container.setLayout(new GridLayout(1, false));
		
		tabs = new CTabFolder(container, SWT.BORDER);
		GridData gd_tabs = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tabs.minimumWidth = -1;
		gd_tabs.minimumHeight = -1;
		tabs.setLayoutData(gd_tabs);
		tabs.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));
		
		tab_general = new CTabItem(tabs, SWT.NONE);
		tab_general.setImage(new Image(null, ExportWizardPage2.class.getResourceAsStream("/ide/appimage/export/appimage_16.png")));
		tab_general.setText("General Settings");
		
		cmp_general = new Composite(tabs, SWT.NONE);
		tab_general.setControl(cmp_general);
		GridLayout gl_cmp_general = new GridLayout(3, false);
		gl_cmp_general.marginTop = 15;
		gl_cmp_general.marginRight = 15;
		gl_cmp_general.marginLeft = 15;
		gl_cmp_general.marginBottom = 15;
		gl_cmp_general.horizontalSpacing = 15;
		gl_cmp_general.verticalSpacing = 15;
		cmp_general.setLayout(gl_cmp_general);
		
		Label lblNewLabel = new Label(cmp_general, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel.setText("Application Name:");
		
		txt_name = new Text(cmp_general, SWT.BORDER);
		txt_name.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(cmp_general, SWT.NONE);
		
		Label lblNewLabel_1 = new Label(cmp_general, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_1.setText("Build Version:");
		
		txt_version = new Text(cmp_general, SWT.BORDER);
		txt_version.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(cmp_general, SWT.NONE);
		
		Label lblNewLabel_2 = new Label(cmp_general, SWT.NONE);
		lblNewLabel_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_2.setText("Application Icon:");
		
		txt_icon = new Text(cmp_general, SWT.BORDER);
		txt_icon.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Button btn_browse_icon = new Button(cmp_general, SWT.NONE);
		btn_browse_icon.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(getShell());
				dialog.setText("Select Integration Icon");
				dialog.setFilterExtensions(new String[] {"*.png"});
				dialog.setFilterNames(new String[] {".png images"});
				dialog.setFilterPath(project_path);
				txt_icon.setText(dialog.open().trim());
			}
		});
		btn_browse_icon.setToolTipText("Browse");
		btn_browse_icon.setImage(new Image(null, ExportWizardPage2.class.getResourceAsStream("/ide/appimage/export/folder.png")));
		
		Label lblNewLabel_3 = new Label(cmp_general, SWT.NONE);
		lblNewLabel_3.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_3.setText("Main Class:");
		
		txt_main_class = new Text(cmp_general, SWT.BORDER);
		txt_main_class.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(cmp_general, SWT.NONE);
		
		Label lblNewLabel_4 = new Label(cmp_general, SWT.NONE);
		lblNewLabel_4.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_4.setText("Output Directory:");
		
		txt_output_dir = new Text(cmp_general, SWT.BORDER);
		txt_output_dir.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Button btn_browse_output_dir = new Button(cmp_general, SWT.NONE);
		btn_browse_output_dir.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog dialog = new DirectoryDialog(getShell());
				dialog.setText("Select Export Directory");
				dialog.setFilterPath(project_path);
				txt_output_dir.setText(dialog.open().trim() + System.getProperty("file.separator") + "appimage_export");
			}
		});
		btn_browse_output_dir.setToolTipText("Browse");
		btn_browse_output_dir.setImage(new Image(null, ExportWizardPage2.class.getResourceAsStream("/ide/appimage/export/folder.png")));
		
		Label lblNewLabel_5 = new Label(cmp_general, SWT.NONE);
		lblNewLabel_5.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_5.setText("JVM Integration:");
		
		txt_integration = new Combo(cmp_general, SWT.NONE);
		txt_integration.setItems(new String[] {"None", "JRE Integration", "JDK Integration"});
		txt_integration.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txt_integration.setText("None");
		new Label(cmp_general, SWT.NONE);
		
		tab_classpath = new CTabItem(tabs, SWT.NONE);
		tab_classpath.setImage(new Image(null, ExportWizardPage2.class.getResourceAsStream("/ide/appimage/export/jar.png")));
		tab_classpath.setText("Classpath Dependencies");
		
		cmp_classpath = new Composite(tabs, SWT.NONE);
		tab_classpath.setControl(cmp_classpath);
		cmp_classpath.setLayout(new FormLayout());
		
		tree_classpath = new Tree(cmp_classpath, SWT.BORDER | SWT.CHECK | SWT.FULL_SELECTION);
		FormData fd_tree_classpath = new FormData();
		fd_tree_classpath.bottom = new FormAttachment(0, 289);
		fd_tree_classpath.top = new FormAttachment(0, 10);
		fd_tree_classpath.left = new FormAttachment(0, 10);
		tree_classpath.setLayoutData(fd_tree_classpath);
		tree_classpath.setLayoutDeferred(true);
		
		TreeColumn trclmnNewColumn = new TreeColumn(tree_classpath, SWT.NONE);
		trclmnNewColumn.setWidth(100);
		trclmnNewColumn.setText("Dependency References");
		
		Button btn_ref_add = new Button(cmp_classpath, SWT.NONE);
		btn_ref_add.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(getShell());
				dialog.setFilterExtensions(new String[] {"*.jar"});
				dialog.setText("Add external reference");
				dialog.setFilterNames(new String[] {".jar files"});
				String ref_path = dialog.open();
				if(ref_path != null) {
					if(!hasRefPath(ref_path)) {
						TreeItem item = new TreeItem(tree_classpath, SWT.NONE);
						item.setText(ref_path);
						item.setData("path",ref_path);
						item.setData("reset_path",ref_path);
						item.setData("reset_text",ref_path);
						item.setBackground(new Color(null, 255, 215, 0));
						item.setChecked(true);
					}else {
						MessageDialog.openWarning(getShell(), "Reference Duplicate", "Reference already exists. Please select a different reference.");
					}					
				}
			}
		});
		fd_tree_classpath.right = new FormAttachment(btn_ref_add, -6);
		FormData fd_btn_ref_add = new FormData();
		fd_btn_ref_add.top = new FormAttachment(0, 10);
		fd_btn_ref_add.right = new FormAttachment(100, -10);
		btn_ref_add.setLayoutData(fd_btn_ref_add);
		btn_ref_add.setImage(new Image(null, ExportWizardPage2.class.getResourceAsStream("/ide/appimage/export/package_add.png")));
		btn_ref_add.setToolTipText("Add external reference");
		
		Button brn_ref_remove = new Button(cmp_classpath, SWT.NONE);
		brn_ref_remove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tree_classpath.getSelectionCount() > 0) {
					if(MessageDialog.openQuestion(getShell(), "Verification", "Please verify that you wish to delete the selected reference.\nPath: " + tree_classpath.getSelection()[0].getText()) == true) {
						tree_classpath.getSelection()[0].dispose();
					}
				}else {
					MessageDialog.openWarning(getShell(), "No Selection", "Please select a reference first.");
				}
			}
		});
		FormData fd_brn_ref_remove = new FormData();
		fd_brn_ref_remove.top = new FormAttachment(btn_ref_add, 6);
		fd_brn_ref_remove.right = new FormAttachment(btn_ref_add, 0, SWT.RIGHT);
		brn_ref_remove.setLayoutData(fd_brn_ref_remove);
		brn_ref_remove.setImage(new Image(null, ExportWizardPage2.class.getResourceAsStream("/ide/appimage/export/package_delete.png")));
		brn_ref_remove.setToolTipText("Remove selected reference");
		
		Button btn_ref_link = new Button(cmp_classpath, SWT.NONE);
		btn_ref_link.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tree_classpath.getSelectionCount() > 0) {
					FileDialog dialog = new FileDialog(getShell());
					dialog.setFilterExtensions(new String[] {"*.jar"});
					dialog.setText("Link to external reference");
					dialog.setFilterNames(new String[] {".jar files"});
					String ref_path = dialog.open();
					if(ref_path != null) {
						if(!hasRefPath(ref_path)) {
							tree_classpath.getSelection()[0].setText(ref_path);
							tree_classpath.getSelection()[0].setData("path", ref_path);
						}else {
							MessageDialog.openWarning(getShell(), "Reference Duplicate", "Reference already exists. Please select a different reference.");
						}
					}
				}else {
					MessageDialog.openWarning(getShell(), "No Selection", "Please select a reference first.");
				}
			}
		});
		FormData fd_btn_ref_link = new FormData();
		fd_btn_ref_link.top = new FormAttachment(brn_ref_remove, 6);
		fd_btn_ref_link.right = new FormAttachment(btn_ref_add, 0, SWT.RIGHT);
		btn_ref_link.setLayoutData(fd_btn_ref_link);
		btn_ref_link.setImage(new Image(null, ExportWizardPage2.class.getResourceAsStream("/ide/appimage/export/package_link.png")));
		btn_ref_link.setToolTipText("Link reference to external library");
		
		Button btn_ref_unlink = new Button(cmp_classpath, SWT.NONE);
		btn_ref_unlink.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tree_classpath.getSelectionCount() > 0) {
					tree_classpath.getSelection()[0].setText((String) tree_classpath.getSelection()[0].getData("reset_text"));
					tree_classpath.getSelection()[0].setData("path",(String) tree_classpath.getSelection()[0].getData("reset_path"));
				}else {
					MessageDialog.openWarning(getShell(), "No Selection", "Please select a reference first.");
				}
			}
		});
		FormData fd_btn_ref_unlink = new FormData();
		fd_btn_ref_unlink.top = new FormAttachment(btn_ref_link, 6);
		fd_btn_ref_unlink.right = new FormAttachment(btn_ref_add, 0, SWT.RIGHT);
		btn_ref_unlink.setLayoutData(fd_btn_ref_unlink);
		btn_ref_unlink.setImage(new Image(null, ExportWizardPage2.class.getResourceAsStream("/ide/appimage/export/package_unlink.png")));
		btn_ref_unlink.setToolTipText("Unlink reference and restore to default");
		
		populate();
	}
	
	public void populate() {
		addToVerification(txt_icon, txt_main_class, txt_name, txt_output_dir, txt_version);
		txt_name.setText(project_name);
		txt_output_dir.setText(project_path + "appimage_build");
		txt_version.setText("0.0.0");
		txt_icon.setText(project_path + "icon.png");
		JSONArray classpath_data = XMLConverter.toJSONObject(Activator.readTextFile(project_path + ".classpath")).getJSONObject("classpath").getJSONArray("classpathentry");
		for(JSONObject reference : classpath_data.toArray(JSONObject.class)) {
			if(reference.getString("kind").equalsIgnoreCase("src")) {
				var_src = project_path + reference.getString("path");
			}else if(reference.getString("kind").equalsIgnoreCase("lib")) {
				TreeItem item = new TreeItem(tree_classpath, SWT.NONE);
				item.setChecked(true);
				item.setText(reference.getString("path"));
				item.setData("path", project_path + reference.getString("path"));
				item.setData("reset_path", project_path + reference.getString("path"));
				item.setData("reset_text", reference.getString("path"));
			}
		}
		if(tree_classpath.getItemCount() > 0) {
			tree_classpath.setSelection(tree_classpath.getItem(0));
		}
		tabs.setSelection(0);				
	}
	
	private void addToVerification(Text... controls) {
		for(Text control : controls) {
			control.addModifyListener(new ModifyListener() {
				@Override
				public void modifyText(ModifyEvent event) {
					setPageComplete(verify(txt_icon) && verify(txt_main_class) && verify(txt_name) && verify(txt_output_dir) && verify(txt_version));
				}
			});
		}
	}
	
	private boolean hasRefPath(String ref_path) {
		boolean output = false;
		for(int i = 0; i < tree_classpath.getItemCount(); i ++) {
			if(ref_path.trim().equalsIgnoreCase(((String) tree_classpath.getItem(i).getData("path")).trim())) {
				output = true;
			}
		}
		return output;
	}
	
	private boolean verify(Text control) {
		return control.getText().trim().length() > 0;
	}
	
	public JSONObject getBuildValues() {
		JSONObject output = new JSONObject();
		output.put("PROJECT_DIR", project_path);
		output.put("NAME", txt_name.getText().trim());
		output.put("VERSION", txt_version.getText().trim());
		output.put("ARCH", architecture);
		output.put("ICON", txt_icon.getText().trim());
		output.put("OUTPUT_DIR", txt_output_dir.getText().trim());
		output.put("MAIN", txt_main_class.getText().trim());
		output.put("SRC", var_src);
		output.put("ASSET", Activator.getPluginPath() + "asset");
		
		if(txt_integration.getText().trim().equalsIgnoreCase("JRE Integration")) {
			output.put("JVM_BOOT_CMD", "${line.separator}export JAVA_HOME=$(pwd)/jvm${line.separator}export PATH=$JAVA_HOME/bin:$PATH${line.separator}");
			output.put("JVM_BUILD_CMD", "cp -rL " + System.getProperty("java.home") + " ${OUTPUT_DIR}/usr/jvm");
		}else if(txt_integration.getText().trim().equalsIgnoreCase("JDK Integration")) {
			output.put("JVM_BOOT_CMD", "${line.separator}export JAVA_HOME=$(pwd)/jvm${line.separator}export PATH=$JAVA_HOME/bin:$PATH${line.separator}");
			output.put("JVM_BUILD_CMD", "cp -rL " + System.getProperty("java.home").substring(0, System.getProperty("java.home").length()-4) + " ${OUTPUT_DIR}/usr/jvm");
		}else {
			output.put("JVM_BOOT_CMD", "");
			output.put("JVM_BUILD_CMD", "");
		}
		
		String copy_jars = "";
		for(TreeItem item : tree_classpath.getItems()) {
			if(item.getChecked()) {
				copy_jars = copy_jars + "<copy file=\"" + String.valueOf(item.getData("path")).trim() + "\" todir=\"${OUTPUT_DIR}/lib\"/>" + System.getProperty("line.separator");
			}			
		}
		output.put("COPY_JARS", copy_jars.trim());
		return output;
	}
	
	public void log(String message) {
		log = log + message + System.getProperty("line.separator");
	}
	
	public String getLog() {
		return log;
	}
	
	public static void touch(String path, String content, Boolean force) throws IOException {
		boolean check = false;
		if(check(path)) {
			if(force == true) {
				delete(path);
				check = true;
			}
		}else {
			check = true;
		}
		if(check == true) {
			if(Paths.get(path).getParent() != null) {
				Files.createDirectories(Paths.get(path).getParent());
			}			
			Files.createFile(Paths.get(path));
			if(content == null) {
				content = "";
			}
			PrintWriter writer = new PrintWriter(new File(path));
			writer.print(content);
			writer.close();
		}
	}
	
	public boolean build() {
		try {
			JSONObject build_values = getBuildValues();
			log("Setting-up build properties.");
			String content = new String(Files.readAllBytes(Paths.get(build_values.getString("ASSET") + System.getProperty("file.separator") + "build.appimage.xml")));
			content = content.replace("#PROJECT_DIR#", build_values.getString("PROJECT_DIR"));
			content = content.replace("#NAME#", build_values.getString("NAME"));
			content = content.replace("#VERSION#", build_values.getString("VERSION"));
			content = content.replace("#ARCH#", build_values.getString("ARCH"));
			content = content.replace("#ICON#", build_values.getString("ICON"));
			content = content.replace("#OUTPUT_DIR#", build_values.getString("OUTPUT_DIR"));
			content = content.replace("#MAIN#", build_values.getString("MAIN"));
			content = content.replace("#SRC#", build_values.getString("SRC"));
			content = content.replace("#ASSET#", build_values.getString("ASSET"));
			content = content.replace("<COPY_JARS/>", build_values.getString("COPY_JARS"));
			content = content.replace("#JVM_BOOT_CMD#", build_values.getString("JVM_BOOT_CMD"));
			content = content.replace("#JVM_BUILD_CMD#", build_values.getString("JVM_BUILD_CMD"));
			
			log("Creating build file.");
			touch(build_values.getString("OUTPUT_DIR") + System.getProperty("file.separator") + "build.xml", content, true);
			
			log("Creating build project.");
			Project build_project = new Project();
			build_project.setUserProperty("ant.file", build_values.getString("OUTPUT_DIR") + System.getProperty("file.separator") + "build.xml");
			build_project.init();
			
			log("Setting-up build monitor.");
			ProjectHelper build_project_helper = ProjectHelper.getProjectHelper();
			build_project.addReference("ant.projectHelper", build_project_helper);
			build_project_helper.parse(build_project, new File(build_values.getString("OUTPUT_DIR") + System.getProperty("file.separator") + "build.xml"));
			
			log("Initiating build process.");
			
			log(build_project.getTargets().get("create_dirs").getDescription());
			build_project.getTargets().get("create_dirs").execute();
			
			log(build_project.getTargets().get("copy_java_deps").getDescription());
			build_project.getTargets().get("copy_java_deps").execute();
			
			log(build_project.getTargets().get("compile_sources").getDescription());
			build_project.getTargets().get("compile_sources").execute();
			
			log(build_project.getTargets().get("compile_scripts").getDescription());
			build_project.getTargets().get("compile_scripts").execute();
			
			log(build_project.getTargets().get("copy_appimage_deps").getDescription());
			build_project.getTargets().get("copy_appimage_deps").execute();
			
			log(build_project.getTargets().get("build_appimage").getDescription());
			build_project.getTargets().get("build_appimage").execute();
			
			log(build_project.getTargets().get("clean").getDescription());
			build_project.getTargets().get("clean").execute();
			
			log("Build Completed.");
			log("");
			log("");
			return true;
		} catch (Exception exception) {
			MessageDialog.openError(getShell(), "Error", exception.getMessage());
			StringWriter string_writer = new StringWriter();
			exception.printStackTrace(new PrintWriter(string_writer));
			String exceptionAsString = string_writer.toString();
			try {
				touch(txt_output_dir.getText() + System.getProperty("file.separator") + "error_log.txt", exceptionAsString, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}
	}
	
	public static boolean check(String path) {
		return Files.exists(Paths.get(path));
	}
	
	public static void delete(String path) throws IOException {
		if(check(path)) {
			Files.delete(Paths.get(path));
		}
	}
}
