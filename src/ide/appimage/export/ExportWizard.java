package ide.appimage.export;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;


public class ExportWizard extends Wizard implements IExportWizard{

	ExportWizardPage page;
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setWindowTitle("Export AppImage");
		setNeedsProgressMonitor(false);
		String project_path = Activator.getProjectPath();
		page = new ExportWizardPage(project_path, project_path.split(System.getProperty("file.separator"))[project_path.split(System.getProperty("file.separator")).length-1].trim());
	}

	@Override
	public boolean performFinish() {
		boolean ok = page.build();
		if(ok) {
			MessageDialog.openInformation(getShell(), "Build Completed", "AppImage build process has completed." + System.getProperty("line.separator") + page.getLog());
		}else {
			MessageDialog.openError(getShell(), "Build Failed", "AppImage build process has failed." + System.getProperty("line.separator") + page.getLog());
		}
		return true;
	}
	
	@Override
	public void addPages() {
		addPage(page);
		
	}
	
	

}
