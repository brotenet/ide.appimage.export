Eclipse plug-in for exporting a 64-bit Linux AppImage package of a desktop-application project. The plug-in ses ANT and can also export the build.xml script for the application.

**Download:** https://gitlab.com/brotenet/ide.appimage.export/tags/Current

![Selection_027](/uploads/5c16640ecb25ab1283af8a891bbf9fe2/Selection_027.png)

**Installation:** Place the ide.export.appimage_...jar file in Eclipse plug-ins directory

----

![Selection_024](/uploads/bb354c2f0238cc1cd15e70c8f66a96dc/Selection_024.png)

Once installed, the plug-in will be available under the 'Java' export folder.

----

![Selection_025](/uploads/ffcceec2e0d89cad1f247c8b9a4f78fe/Selection_025.png)

It is required to at least set the application icon path and the class-path where the 'main' method resides.

the 'JVM integration' option allows embedding of the actively used JRE or JDK.

----

![Selection_026](/uploads/9fad5d71b822044536ac55b70c26dcef/Selection_026.png) 

The 'Dependency References' tab allows to add/remove and override .jar dependencies.